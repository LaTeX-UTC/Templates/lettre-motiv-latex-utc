Template LaTeX : Lettre de motivation simple et moderne avec logo UTC
=====

- [Utilisation](#utilisation)
- [Licence](#licence)
- [Prévisualisation](#pr%C3%A9visualisation)
- [Contribution](#contribution)


## Utilisation
:warning: **Pour une utilisation simple, ne téléchargez pas le _repo_ !** Téléchargez l'archive prête à être utilisée : [ici](https://latex-utc.gitlab.utc.fr/Templates/lettre-motiv-latex-utc/latex-lettre-motivation-UTC.zip) ! :warning:

🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝🔝

💡 Cette archive peut être directement utilisée sur [Overleaf](https://www.overleaf.com/). :seedling:

_Elle est systématiquement préparée en intégration continue à chaque commit ; si les liens venaient à périmer, n'hésitez pas à demander un renouvellement._

**Remarque**: Ce template utilise le package [scrlttr2](https://ctan.org/pkg/scrlttr2). Si vous souhaitez modifier certaines caractéristiques du template, je vous invite à lire la [documentation](http://mirrors.ctan.org/macros/latex/contrib/koma-script/doc/scrguien.pdf) 
qui vous permettra de modifier très facilement le template (notamment la taille des blocs d'adresse qui prend trop de place selon certaines âmes sensibles).

## Licence

Le contenu de ce _repo_ est distribué sous licence BSD-2. _Attention l'archive dont nous parlons au-dessus contient des éléments graphiques de [ce repo](https://gitlab.utc.fr/LaTeX-UTC/Graphismes-UTC) dont certains sont de la propriété de l'université de technologie de Compiègne. Plus d'informations sont disponibles sur le repo en question._



## Prévisualisation 

![Page](https://latex-utc.gitlab.utc.fr/Templates/lettre-motiv-latex-utc/main.png)

[Lien vers le `PDF` généré](https://latex-utc.gitlab.utc.fr/Templates/lettre-motiv-latex-utc/main.pdf).


## Contribution

Toute contribution est la bienvenue.